#!/bin/bash
set -e

# Example
# cat hosts >> /etc/hosts
# service ssh start

exec "$@ ; tail -f /dev/null"


