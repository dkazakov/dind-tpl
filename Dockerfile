FROM debian:9

# Libraries
# TODO: от openssh-server в будущем нужно отказаться (нужно только для тестов)
RUN apt-get update && apt-get -y install \
    sudo \
    bc \
    locales \
    locales-all \
    mc \
    openssh-server \
    openssh-client \
    net-tools \
    netcat \
    nmap \
    rsync \
    wget \
    zip \
    sqlite3 \
    \
    build-essential \
    cmake \
    pkg-config \
    libboost-dev \
    libboost-log-dev \
    libboost-test-dev \
    libboost-program-options-dev \
    libatlas-base-dev \
    libatlas-dev \
    libblas-dev \
    liblapack-dev \
    libtbb-dev \
    libsqlite3-dev \
    libmsgpack-dev \
    libssl-dev \
    librdkafka-dev \
    rapidjson-dev \
    clang-format-3.9 \
    colordiff \
    libspdlog-dev \
    libsparsehash-dev \
    libboost-iostreams-dev \
    \
    python3 \
    python3-venv \
    python3-pip \
    \
    libffi-dev \
    kafkacat \
    default-libmysqlclient-dev \
    rsyslog-kafka \
    default-jre \
    \
    git \
    \
    ipvsadm \
    && rm -rf /var/lib/apt/lists/*

# Locales
ENV LANG='en_US.UTF-8' \
    LANGUAGE='en_US.UTF-8' \
    LC_ALL='en_US.UTF-8'

RUN ssh-keygen -A && \
    \
    ssh-keygen -t rsa -N '' -f ~/.ssh/id_rsa && \
    cp ~/.ssh/id_rsa.pub ~/.ssh/authorized_keys && \
    bash -c 'echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config' && \
    chmod g-w ~/.ssh/config && \
    \

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

